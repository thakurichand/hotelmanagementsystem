﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelBooking.Models;
using PagedList;

namespace HotelBooking.Controllers
{
    public class RoomTypesController : Controller
    {
        private HotelDbContext db = new HotelDbContext();

        // GET: RoomTypes
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.RoomNameSortParm = sortOrder == "RoomName" ? "room_name" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            var RoomTypes = from r in db.RoomType select r;
            if (!string.IsNullOrEmpty(searchString))
            {
                RoomTypes = RoomTypes.Where(r => r.RoomName.Contains(searchString) );

            }
            switch (sortOrder)
            {
                case "room_name":
                    RoomTypes = RoomTypes.OrderByDescending(r => r.RoomName);
                    break;
                default:
                    RoomTypes = RoomTypes.OrderBy(r => r.RoomName);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(RoomTypes.ToPagedList(pageNumber, pageSize));
        }

        // GET: RoomTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomTypes roomTypes = db.RoomType.Find(id);
            if (roomTypes == null)
            {
                return HttpNotFound();
            }
            return View(roomTypes);
        }

        // GET: RoomTypes/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: RoomTypes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "RoomTypeId,RoomName")] RoomTypes roomTypes)
        {
            if (ModelState.IsValid)
            {
                db.RoomType.Add(roomTypes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(roomTypes);
        }

        // GET: RoomTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomTypes roomTypes = db.RoomType.Find(id);
            if (roomTypes == null)
            {
                return HttpNotFound();
            }
            return View(roomTypes);
        }

        // POST: RoomTypes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "RoomTypeId,RoomName")] RoomTypes roomTypes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roomTypes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(roomTypes);
        }

        // GET: RoomTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            RoomTypes roomTypes = db.RoomType.Find(id);
            if (roomTypes == null)
            {
                return HttpNotFound();
            }
            return View(roomTypes);
        }

        // POST: RoomTypes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            RoomTypes roomTypes = db.RoomType.Find(id);
            db.RoomType.Remove(roomTypes);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
