﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelBooking.Models;
using PagedList;

namespace HotelBooking.Controllers
{
    public class HotelsController : Controller
    {
        private HotelDbContext db = new HotelDbContext();

        // GET: Hotels
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {

            ViewBag.CurrentSort = sortOrder;
            ViewBag.HotelNameSortParm = sortOrder == "HotelName" ? "hotel_name" : "name";
            ViewBag.LocationSortparm = sortOrder == "Location" ? "hotel_location" : "location";
            ViewBag.PhoneNoSortParm = sortOrder == "PhoneNo" ? "hotel_phoneno" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            var Hotels = from s in db.Hotel select s;
            if (!string.IsNullOrEmpty(searchString))
            {
                Hotels = Hotels.Where(s => s.HotelName.Contains(searchString) || s.Location.Contains(searchString) || s.PhoneNo.Contains(searchString));

            }
            switch (sortOrder)
            {
                case "name":
                    Hotels = Hotels.OrderBy(s => s.HotelName);
                    break;
                case "hotel_name":

                    Hotels = Hotels.OrderByDescending(s => s.HotelName);
                    break;
                case "location":
                    Hotels = Hotels.OrderBy(s => s.Location);
                    break;
                case "hotel_location":

                    Hotels = Hotels.OrderByDescending(s => s.Location);
                    break;


                case "hotel_phoneno":
                    Hotels = Hotels.OrderByDescending(s => s.PhoneNo);
                    break;
                default:
                    Hotels = Hotels.OrderBy(s => s.PhoneNo);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(Hotels.ToPagedList(pageNumber, pageSize));
        }

        // GET: Hotels/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotels hotels = db.Hotel.Find(id);
            if (hotels == null)
            {
                return HttpNotFound();
            }
            return View(hotels);
        }

        // GET: Hotels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Hotels/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "HotelId,HotelName,Location,PhoneNo")] Hotels hotels)
        {
            if (ModelState.IsValid)
            {
                db.Hotel.Add(hotels);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(hotels);
        }

        // GET: Hotels/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotels hotels = db.Hotel.Find(id);
            if (hotels == null)
            {
                return HttpNotFound();
            }
            return View(hotels);
        }

        // POST: Hotels/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "HotelId,HotelName,Location,PhoneNo")] Hotels hotels)
        {
            if (ModelState.IsValid)
            {
                db.Entry(hotels).State = EntityState.Modified;               
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(hotels);
        }

        // GET: Hotels/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Hotels hotels = db.Hotel.Find(id);
            if (hotels == null)
            {
                return HttpNotFound();
            }
            return View(hotels);
        }

        // POST: Hotels/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Hotels hotels = db.Hotel.Find(id);
            db.Hotel.Remove(hotels);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
