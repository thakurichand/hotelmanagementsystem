﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelBooking.Models;
using PagedList;

namespace HotelBooking.Controllers
{
    public class GuestsController : Controller
    {
        private HotelDbContext db = new HotelDbContext();

        // GET: Guests
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.FullNameSortParm = sortOrder == "FullName" ? "full_name" : "name";
            ViewBag.PhoneNoSortParm = sortOrder == "PhoneNo" ? "hotel_phoneno" : "number";
            ViewBag.AddressSortParm = sortOrder == "Address" ? "address" : "";
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            var Guests = from g in db.Guest select g;
            if (!string.IsNullOrEmpty(searchString))
            {
                Guests = Guests.Where(g => g.FullName.Contains(searchString)|| g.PhoneNo.Contains(searchString)||g.Address.Contains(searchString));

            }
            switch (sortOrder)
            {
                case "name":
                    Guests = Guests.OrderBy(g => g.FullName);
                    break;
                case "full_name":

                    Guests = Guests.OrderByDescending(g => g.FullName);
                    break;
               
                case "number":
                    Guests = Guests.OrderBy(s => s.PhoneNo);
                    break;
                case "hotel_phoneno":

                    Guests = Guests.OrderByDescending(s => s.PhoneNo);
                    break;


                case "address":
                    Guests = Guests.OrderByDescending(s => s.Address);
                    break;
                default:
                    Guests = Guests.OrderBy(s => s.Address);
                    break;
            }
            int pageSize = 5;
            int pageNumber = (page ?? 1);
            return View(Guests.ToPagedList(pageNumber, pageSize));
        }

        // GET: Guests/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guests guests = db.Guest.Find(id);
            if (guests == null)
            {
                return HttpNotFound();
            }
            return View(guests);
        }

        // GET: Guests/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Guests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "GuestId,FirstName,LastName,PhoneNo,Address")] Guests guests)
        {
            if (ModelState.IsValid)
            {
                db.Guest.Add(guests);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(guests);
        }

        // GET: Guests/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guests guests = db.Guest.Find(id);
            if (guests == null)
            {
                return HttpNotFound();
            }
            return View(guests);
        }

        // POST: Guests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "GuestId,FirstName,LastName,PhoneNo,Address")] Guests guests)
        {
            if (ModelState.IsValid)
            {
                db.Entry(guests).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(guests);
        }

        // GET: Guests/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Guests guests = db.Guest.Find(id);
            if (guests == null)
            {
                return HttpNotFound();
            }
            return View(guests);
        }

        // POST: Guests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Guests guests = db.Guest.Find(id);
            db.Guest.Remove(guests);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
