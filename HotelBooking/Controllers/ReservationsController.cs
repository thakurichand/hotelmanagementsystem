﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HotelBooking.Models;
using HotelBooking.ViewModels;

namespace HotelBooking.Controllers
{
    public class ReservationsController : Controller
    {
        private HotelDbContext db = new HotelDbContext();

        // GET: Reservations
        public ActionResult Index()
        {
            List<ReservationsViewModels> reservationsViewModels = new List<ReservationsViewModels>();
            var reservations = (from r in db.Reservation
                                join g in db.Guest on r.ReservationId equals g.GuestId
                                join ro in db.Room on r.ReservationId equals ro.RoomId
                                join rt in db.RoomType on r.ReservationId equals rt.RoomTypeId
                                join h in db.Hotel on r.ReservationId equals h.HotelId
                                select new { g.FirstName, ro.RoomPrice, rt.RoomName, h.HotelName, r.ArrivalDate, r.DepartureDate, r.IsConformed }).ToList();
            foreach (var iteam in reservations)
            {
                ReservationsViewModels reservationsView = new ReservationsViewModels
                {
                    FirstName = iteam.FirstName,
                    RoomPrice = iteam.RoomPrice,
                    RoomName = iteam.RoomName,
                    HotelName = iteam.HotelName,
                    ArrivalDate = iteam.ArrivalDate,
                    DepartureDate = iteam.DepartureDate,
                    IsConformed = iteam.IsConformed
                };
                reservationsViewModels.Add(reservationsView);
            }
            return View(reservationsViewModels);
        }

        // GET: Reservations/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservations reservations = db.Reservation.Find(id);
            if (reservations == null)
            {
                return HttpNotFound();
            }
            return View(reservations);
        }

        // GET: Reservations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Reservations/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ReservationId,GuestId,RoomId,RoomTypeId,HotelId,ArrivalDate,DepartureDate,IsConformed")] Reservations reservations)
        {
            if (ModelState.IsValid)
            {
                db.Reservation.Add(reservations);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(reservations);
        }

        // GET: Reservations/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservations reservations = db.Reservation.Find(id);
            if (reservations == null)
            {
                return HttpNotFound();
            }
            return View(reservations);
        }

        // POST: Reservations/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ReservationId,GuestId,RoomId,RoomTypeId,HotelId,ArrivalDate,DepartureDate,IsConformed")] Reservations reservations)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reservations).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(reservations);
        }

        // GET: Reservations/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Reservations reservations = db.Reservation.Find(id);
            if (reservations == null)
            {
                return HttpNotFound();
            }
            return View(reservations);
        }

        // POST: Reservations/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Reservations reservations = db.Reservation.Find(id);
            db.Reservation.Remove(reservations);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
