﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelBooking.Models
{
    public class Guests
    {
        [Key]
        public int GuestId { get; set; }
        [Required(ErrorMessage = "Please Enter FirstName")]
        [StringLength(20)]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "Please Enter LastName")]
        [StringLength(20)]
        public string LastName { get; set; }
        public string FullName { get { return FirstName + " " + LastName; } }
        [Required(ErrorMessage = "Please Enter PhoneNumber")]
        [DisplayName("PhoneNumber")]
        public string PhoneNo { get; set; }
        [Required(ErrorMessage = "Please Enter Address")]
        public string Address { get; set; }
    }
}