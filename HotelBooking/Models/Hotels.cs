﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelBooking.Models
{
    public class Hotels
    {
        [Key]
        public int HotelId { get; set; }
        [Required(ErrorMessage = "Please Enter HotelName")]
        [StringLength(50)]
        public string HotelName { get; set; }
        [Required(ErrorMessage = "Please Enter Location")]
        [StringLength(20)]
        public string Location { get; set; }
        [Required(ErrorMessage = "Please Enter PhoneNumber")]
        [DisplayName("PhoneNumber")]
        public string PhoneNo { get; set; }
    }
}