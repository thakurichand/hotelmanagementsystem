﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelBooking.Models
{
    public class Rooms
    {
        [Key]
        public int RoomId { get; set; }
        public int RoomTypeId { get; set; }
        public int HotelId { get; set; }
        [Required(ErrorMessage = "Please Enter Price")]
        [Range(300, 20000)]
        public decimal RoomPrice { get; set; }
        [Required(ErrorMessage = "Please Enter TotalRoom")]
        public int TotalRoom { get; set; }
    }
}