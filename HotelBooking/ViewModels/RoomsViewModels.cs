﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HotelBooking.ViewModels
{
    public class RoomsViewModels
    {
        public string RoomName { get; set; }
        public string HotelName { get; set; }
        public decimal RoomPrice { get; set; }
        public int TotalRoom { get; set; }
    }
}