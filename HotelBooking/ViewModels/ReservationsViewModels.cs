﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace HotelBooking.ViewModels
{
    public class ReservationsViewModels
    {
        public string FirstName { get; set; }
        public decimal RoomPrice { get; set; }
        public string RoomName { get; set; }
        public string HotelName { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime ArrivalDate { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime DepartureDate { get; set; }
        public bool IsConformed { get; set; }
    }
}